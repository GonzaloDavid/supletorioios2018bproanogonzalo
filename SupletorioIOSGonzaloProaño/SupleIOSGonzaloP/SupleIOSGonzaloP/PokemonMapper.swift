//
//  PokemonMapper.swift
//  SupleIOSGonzaloP
//
//  Created by Gonzalo David Proaño on 15/2/19.
//  Copyright © 2019 S. All rights reserved.
//

import Foundation
import ObjectMapper

class pokemonStructure:Mappable {
   
    var base_experience:String?
    var height:String?
    var weight:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        base_experience <- map["base_experience"]
        height <- map["height"]
        weight <- map["weight"]
    }
    
}


