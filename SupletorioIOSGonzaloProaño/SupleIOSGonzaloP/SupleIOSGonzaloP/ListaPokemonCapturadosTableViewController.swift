//
//  ListaPokemonCapturadosTableViewController.swift
//  SupleIOSGonzaloP
//
//  Created by Gonzalo David Proaño on 15/2/19.
//  Copyright © 2019 S. All rights reserved.
//

import UIKit
import FirebaseAuth
import AlamofireObjectMapper
import Alamofire
import FirebaseDatabase


class ListaPokemonCapturadosTableViewController:  UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var labelprueba: UILabel!
    
    var ref: DatabaseReference!
    var pokemones:[[String:String]] = []
    
    @IBOutlet weak var tableviewlistapokemones: UITableView!
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
       
        getInfoFireBase()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return pokemones.count
    }
    func getInfoFireBase(){
        let pokemonRef = ref.child("pokemon")
        print("metodo get data")
       
        pokemonRef.observe(.childAdded) { (dataSnapshot) in
            let pokemonDic = dataSnapshot.value as? [String:String] ?? [:]
            self.pokemones += [pokemonDic]
            self.tableviewlistapokemones.reloadData()
             print(self.pokemones)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let alto = pokemones[indexPath.row]["alto"] ?? "alto"
        let experiencia = pokemones[indexPath.row]["experiencia"] ?? "experiencia"
        let ancho = pokemones[indexPath.row]["ancho"] ?? "ancho"
        cell.textLabel?.text = "\(alto) - \(experiencia)  - \(ancho)"
        
        print("valor de celda--->")
        print(cell)
        
        return cell
    }

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
