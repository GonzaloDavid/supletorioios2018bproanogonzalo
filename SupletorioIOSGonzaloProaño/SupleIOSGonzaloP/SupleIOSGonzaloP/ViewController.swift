//
//  ViewController.swift
//  SupleIOSGonzaloP
//
//  Created by Gonzalo David Proaño on 15/2/19.
//  Copyright © 2019 S. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
import FirebaseAuth
import Toast_Swift
class ViewController: UIViewController {

    @IBOutlet weak var usertextfield: UITextField!
    @IBOutlet weak var passwordtextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func ingresarbuttonPressed(_ sender: Any) {
        let usuario=usertextfield.text!
        let password=passwordtextfield.text!
        print(usuario)
        print(password)
        
        Auth.auth().signIn(withEmail: usuario, password: password) { (data, error) in
            
            if let error = error {
                print("entro en error")
                self.view.makeToast("ERROR CONTRASEÑA", duration: 3.0, position: .top)
                print(error)
                return
            }
           
            
            print("BIENVENIDO GONZALO")
             self.view.makeToast("bienvenido", duration: 3.0, position: .top)
            
        }
    }
    
}

